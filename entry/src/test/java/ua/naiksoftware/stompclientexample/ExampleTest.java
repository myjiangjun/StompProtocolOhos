package ua.naiksoftware.stompclientexample;

import org.junit.Test;
import ua.naiksoftware.stomp.StompClient;

import static org.mockito.Mockito.mock;

public class ExampleTest {



    StompClient client = mock(StompClient.class);

    @Test
    public void connect() {
        client.connect();
    }

    @Test
    public void lifecycle() {
        client.lifecycle();
    }

    @Test
    public void disconnect() {
        client.disconnect();
    }

    @Test
    public void disconnectCompletable() {
        client.disconnectCompletable();
    }
    @Test
    public void reconnect(){
        client.reconnect();
    }

}
