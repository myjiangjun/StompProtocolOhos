/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ua.naiksoftware.stompclientexample.utils;

import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Context;
import ua.naiksoftware.stompclientexample.ResourceTable;

/**
 * ToastUtil
 *
 * @since 2021-03-29
 */
public class ToastUtil {
    private static final int NUM_5000 = 5000;
    private static final int NUM_58 = 58;
    private Context context;

    /**
     * 构造 ToastUtil
     *
     * @param cot
     */
    public ToastUtil(Context cot) {
        this.context = cot;
    }

    /**
     * toastCtx
     *
     * @param ctx
     * @param text
     */
    public static void toastCtx(Context ctx, String text) {
        new ToastDialog(ctx)
                .setText(text)
                .setSize(ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_CONTENT)
                .setAutoClosable(true)
                .setDuration(NUM_5000)
                .setCornerRadius(NUM_58)
                .show();
    }

    /**
     * toastCtx
     *
     * @param ctx
     * @param layout
     */
    public static void toastCtx(Context ctx, Component layout) {
        Component customToastLayout = (Component) LayoutScatter.getInstance(ctx).parse(layout.getId(), null, false);
        ToastDialog toastDialog = new ToastDialog(ctx);
        toastDialog.setComponent(customToastLayout);
        toastDialog.setCornerRadius(NUM_58);
        toastDialog.setSize(DirectionalLayout.LayoutConfig.MATCH_CONTENT, DirectionalLayout.LayoutConfig.MATCH_CONTENT);
        toastDialog.setAlignment(LayoutAlignment.CENTER);
        toastDialog.show();
    }

    /**
     * toast
     *
     * @param text
     */
    public void toast(String text) {
        new ToastDialog(context)
                .setText(text)
                .setSize(ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_CONTENT)
                .setAutoClosable(true)
                .setDuration(NUM_5000)
                .setCornerRadius(NUM_58)
                .show();
    }

    /**
     * toast
     *
     * @param text
     * @param duration
     */
    public void toast(String text, int duration) {
        new ToastDialog(context)
                .setText(text)
                .setSize(ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_CONTENT)
                .setDuration(duration)
                .setCornerRadius(NUM_58)
                .show();
    }

    /**
     * toast
     *
     * @param text
     * @param duration
     * @param offsetX
     * @param offsetY
     * @param gravity
     */
    public void toast(String text, int duration, int offsetX, int offsetY, int gravity) {
        new ToastDialog(context)
                .setText(text)
                .setSize(ComponentContainer.LayoutConfig.MATCH_PARENT,
                        ComponentContainer.LayoutConfig.MATCH_CONTENT)
                .setAlignment(gravity)
                .setDuration(duration)
                .setCornerRadius(NUM_58)
                .setOffset(offsetX, offsetY)
                .show();
    }

    /**
     * void
     *
     * @param context
     * @param text
     */
    public static void toast(Context context, String text) {
        ComponentContainer customToastLayout = (ComponentContainer) LayoutScatter.getInstance(context)
                .parse(ResourceTable.Layout_toast_dialog_layout, null, false);
        Text textView = (Text) customToastLayout.findComponentById(ResourceTable.Id_toast_text);
        textView.setText(text);
        ToastDialog toastDialog = new ToastDialog(context);
        toastDialog.setComponent(customToastLayout);
        toastDialog.setCornerRadius(NUM_58);
        toastDialog.setSize(ComponentContainer.LayoutConfig.MATCH_CONTENT,
                DirectionalLayout.LayoutConfig.MATCH_CONTENT);
        toastDialog.setAlignment(LayoutAlignment.BOTTOM);
        toastDialog.show();
    }
}
