package ua.naiksoftware.stompclientexample;

import io.reactivex.Completable;
import io.reactivex.Observable;
import org.junit.Test;
import ua.naiksoftware.stomp.StompClient;
import ua.naiksoftware.stomp.dto.LifecycleEvent;
import ua.naiksoftware.stomp.provider.ConnectionProvider;


public class ExampleOhosTest {
    // 不涉及 测试报告生成异常
    StompClient client = new StompClient(new ConnectionProvider() {
        @Override
        public Observable<String> messages() {
            return null;
        }

        @Override
        public Completable send(String stompMessage) {
            return null;
        }

        @Override
        public Observable<LifecycleEvent> lifecycle() {
            return null;
        }

        @Override
        public Completable disconnect() {
            return null;
        }
    });
    @Test
    public void connect() {
        client.connect();
    }

    @Test
    public void lifecycle() {
        client.lifecycle();
    }

    @Test
    public void disconnect() {
        client.disconnect();
    }

    @Test
    public void disconnectCompletable() {
        client.disconnectCompletable();
    }
    @Test
    public void reconnect(){
        client.reconnect();
    }
}