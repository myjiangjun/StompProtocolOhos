# StompProtocolOhos

#### 项目介绍
- 项目名称: StompProtocolOhos
- 所属系列：OpenHarmony的第三方组件适配移植
- 功能：对STOMP协议支持长连接 收发消息
- 项目移植状态：完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio 2.2 Beta1
- 基线版本：Release 1.6.6

#### 效果演示
![输入图片说明](https://gitee.com/chinasoft3_ohos/StompProtocolOhos/raw/master/gif/demo.gif  "demo.gif")

#### 安装教程

1.在项目根目录下的build.gradle文件中，
 ```gradle
allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/releases/'
        }
    }
}
 ```

2.在entry模块的build.gradle文件中，

 ```gradle
 dependencies {
      implementation('com.gitee.chinasoft_ohos:StompProtocolOhos:1.0.1')
    ......  
 }
 ```
在sdk6，DevEco Studio 2.2 Beta1下项目可直接运行 如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件， 并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明

1. 后端示例:

**WebSocketConfig.groovy**

	
	@Configuration
	@EnableWebSocket

	@EnableWebSocketMessageBroker

	class WebSocketConfig implements WebSocketMessageBrokerConfigurer {
	
	    @Override
	    void configureMessageBroker(MessageBrokerRegistry config) {
	
	        long[] heartbeat = [ 30000, 30000 ];
	        config.enableSimpleBroker("/topic", "/queue", "/exchange")
	            .setTaskScheduler(new DefaultManagedTaskScheduler()) // enable heartbeats
	            .setHeartbeatValue(heartbeat); // enable heartbeats
	//        config.enableStompBrokerRelay("/topic", "/queue", "/exchange"); // Uncomment for external message broker (ActiveMQ, RabbitMQ)
	        config.setApplicationDestinationPrefixes("/topic", "/queue"); // prefix in client queries
	        config.setUserDestinationPrefix("/user");
	    }
	
	    @Override
	    void registerStompEndpoints(StompEndpointRegistry registry) {
	        registry.addEndpoint("/example-endpoint").setAllowedOrigins("*").withSockJS()
	    }
	
	    @Override
	    void configureWebSocketTransport(WebSocketTransportRegistration registration) {
	        registration.setMessageSizeLimit(8 * 1024);
	    }
	}


	
**SocketController.groovy**



	@Slf4j
	@RestController
	class SocketController {
	
	    @Autowired
	    SocketService socketService
	
	    @MessageMapping('/hello-msg-mapping')
	    @SendTo('/topic/greetings')
	    EchoModel echoMessageMapping(String message) {
	        log.debug("React to hello-msg-mapping")
	        return new EchoModel(message.trim())
	    }
	
	    @RequestMapping(value = '/hello-convert-and-send', method = RequestMethod.POST)
	    void echoConvertAndSend(@RequestParam('msg') String message) {
	        socketService.echoMessage(message)
	    }
	}

2. 示例用法:

**基本用法**

	 private StompClient mStompClient;
	
	 // ...
	 
	 mStompClient = Stomp.over(Stomp.ConnectionProvider.OKHTTP, "ws://" + OhOS_EMULATOR_LOCALHOST + ":" + RestClient.SERVER_PORT + "/example-endpoint/websocket");
	 mStompClient.connect();
	  
		mStompClient.topic("/topic/greetings")
		                .subscribeOn(Schedulers.io())
		                .observeOn(HarmonySchedulers.mainThread())
		                .subscribe(topicMessage -> {
		                    debug("Received " + topicMessage.getPayload());
		                    addItem(mGson.fromJson(topicMessage.getPayload(), EchoModel.class));
		                }, throwable -> {
		                    debug("Error on subscribe topic" + throwable);
		                });
	  
	 // ...

	 mStompClient.disconnect();



. 订阅生命周期连接:

	lifecycleDisposable = connectionProvider.lifecycle()
	                	.subscribe(lifecycleEvent -> {
	                    switch (lifecycleEvent.getType()) {
	                        case OPENED:
	                          HiLog.error(LABEL, "Socket opened");
	                            break;
	
	                        case CLOSED:
	                            HiLog.error(LABEL, "Socket closed");
	                            disconnect();
	                            break;
	
	                        case ERROR:
	                            HiLog.error(LABEL, "Socket closed with error");
	                            lifecyclePublishSubject.onNext(lifecycleEvent);
	                            break;
	                        default:
	                            break;
	                    }
	                });
	

#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

## 版本迭代
- 1.0.1

## 版权和许可信息
  
MIT License