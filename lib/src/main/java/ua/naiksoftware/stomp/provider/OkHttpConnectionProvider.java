package ua.naiksoftware.stomp.provider;


import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import io.reactivex.annotations.NonNull;
import io.reactivex.annotations.Nullable;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import okhttp3.Headers;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;
import okio.ByteString;
import ua.naiksoftware.stomp.dto.LifecycleEvent;

/**
 * OkHttpConnectionProvider
 *
 * @since 2021-04-21
 */
public class OkHttpConnectionProvider extends AbstractConnectionProvider {
    static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00201,
            OkHttpConnectionProvider.class.getSimpleName());
    private static final int NUMBER_1000 = 1000;

    private final String mUri;
    @NonNull
    private final Map<String, String> mConnectHttpHeaders;
    private final OkHttpClient mOkHttpClient;

    @Nullable
    private WebSocket openSocket;

    /**
     * OkHttpConnectionProvider
     *
     * @param uri
     * @param connectHttpHeaders
     * @param okHttpClient
     */
    public OkHttpConnectionProvider(String uri, @Nullable Map<String, String> connectHttpHeaders, OkHttpClient okHttpClient) {
        super();
        mUri = uri;
        mConnectHttpHeaders = connectHttpHeaders != null ? connectHttpHeaders : new HashMap<>();
        mOkHttpClient = okHttpClient;
    }

    /**
     * rawDisconnect
     */
    @Override
    public void rawDisconnect() {
        if (openSocket != null) {
            openSocket.close(NUMBER_1000, "");
        }
    }

    /**
     * createWebSocketConnection
     */
    @Override
    protected void createWebSocketConnection() {
        Request.Builder requestBuilder = new Request.Builder()
                .url(mUri);

        addConnectionHeadersToBuilder(requestBuilder, mConnectHttpHeaders);

        openSocket = mOkHttpClient.newWebSocket(requestBuilder.build(),
                new WebSocketListener() {
                    @Override
                    public void onOpen(WebSocket webSocket, @NonNull Response response) {
                        open(response);
                    }
                    @Override
                    public void onMessage(WebSocket webSocket, String text) {
                        emitMessage(text);
                    }
                    @Override
                    public void onMessage(WebSocket webSocket, @NonNull ByteString bytes) {
                        emitMessage(bytes.utf8());
                    }
                    @Override
                    public void onClosed(WebSocket webSocket, int code, String reason) {
                        closed();
                    }
                    @Override
                    public void onFailure(WebSocket webSocket, Throwable t, Response response) {
                        failure(t);
                    }
                    @Override
                    public void onClosing(final WebSocket webSocket, final int code, final String reason) {
                        webSocket.close(code, reason);
                    }
                }

        );
    }

    private void closed() {
        openSocket = null;
        emitLifecycleEvent(new LifecycleEvent(LifecycleEvent.Type.CLOSED));
    }

    private void failure(Throwable t) {

        String message = t.getMessage();
        debug(message);
        emitLifecycleEvent(new LifecycleEvent(LifecycleEvent.Type.ERROR, new Exception(t)));
        openSocket = null;
        emitLifecycleEvent(new LifecycleEvent(LifecycleEvent.Type.CLOSED));
    }

    private void open(Response response) {

        LifecycleEvent openEvent = new LifecycleEvent(LifecycleEvent.Type.OPENED);

        TreeMap<String, String> headersAsMap = headersAsMap(response);

        openEvent.setHandshakeResponseHeaders(headersAsMap);
        emitLifecycleEvent(openEvent);
    }

    /**
     * rawSend
     *
     * @param stompMessage message to send
     */
    @Override
    protected void rawSend(String stompMessage) {
        openSocket.send(stompMessage);
    }

    /**
     * getSocket
     *
     * @return Object
     */
    @Nullable
    @Override
    protected Object getSocket() {
        return openSocket;
    }

    /**
     * headersAsMap
     *
     * @param response response
     * @return TreeMap
     */
    @NonNull
    private TreeMap<String, String> headersAsMap(@NonNull Response response) {
        TreeMap<String, String> headersAsMap = new TreeMap<>();
        Headers headers = response.headers();
        for (String key : headers.names()) {
            headersAsMap.put(key, headers.get(key));
        }
        return headersAsMap;
    }

    /**
     * addConnectionHeadersToBuilder
     *
     * @param requestBuilder
     * @param connMap
     */
    private void addConnectionHeadersToBuilder(@NonNull Request.Builder requestBuilder, @NonNull Map<String, String> connMap) {
        for (Map.Entry<String, String> headerEntry : connMap.entrySet()) {
            requestBuilder.addHeader(headerEntry.getKey(), headerEntry.getValue());
        }
    }

    private void debug(String message) {
        HiLog.debug(LABEL, message);
    }
}
