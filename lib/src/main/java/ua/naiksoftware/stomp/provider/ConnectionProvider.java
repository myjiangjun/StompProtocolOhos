package ua.naiksoftware.stomp.provider;

import io.reactivex.Completable;
import io.reactivex.Observable;
import ua.naiksoftware.stomp.dto.LifecycleEvent;

/**
 * Created by naik on 05.05.16.
 *
 * @since 2021-04-27
 */
public interface ConnectionProvider {
    /**
     * Subscribe this for receive stomp messages
     * @return Observable
     */
    Observable<String> messages();

    /**
     * Sending stomp messages via you ConnectionProvider.
     * onError if not connected or error detected will be called, or onCompleted id sending started
     * send messages with ACK
     *
     * @param stompMessage stompMessage
     * @return Completable
     * @noinspection checkstyle:TodoComment
     */
    Completable send(String stompMessage);

    /**
     * Subscribe this for receive #LifecycleEvent events
     * @return Observable
     */
    Observable<LifecycleEvent> lifecycle();

    /**
     * Disconnects from server. This is basically a Callable.
     * Automatically emits Lifecycle.CLOSE
     *
     * @return Completable Completable
     */
    Completable disconnect();
}
