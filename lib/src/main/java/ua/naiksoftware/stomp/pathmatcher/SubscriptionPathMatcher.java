package ua.naiksoftware.stomp.pathmatcher;

import ua.naiksoftware.stomp.StompClient;
import ua.naiksoftware.stomp.dto.StompHeader;
import ua.naiksoftware.stomp.dto.StompMessage;

/**
 * SubscriptionPathMatcher
 *
 * @since 2021-04-21
 */
public class SubscriptionPathMatcher implements PathMatcher {
    private final StompClient stompClient;

    /**
     * SubscriptionPathMatcher
     *
     * @param client
     */
    public SubscriptionPathMatcher(StompClient client) {
        this.stompClient = client;
    }

    /**
     * matches
     *
     * @param path path
     * @param msg  msg
     * @return boolean
     */
    @Override
    public boolean matches(String path, StompMessage msg) {
        /**
         * Compare subscription
         */
        String pathSubscription = stompClient.getTopicId(path);
        if (pathSubscription == null) {
            return false;
        } else {
            String subscription = msg.findHeader(StompHeader.SUBSCRIPTION);
            return pathSubscription.equals(subscription);
        }
    }
}
