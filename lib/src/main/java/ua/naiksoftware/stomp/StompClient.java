package ua.naiksoftware.stomp;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Completable;
import io.reactivex.CompletableSource;
import io.reactivex.Flowable;
import io.reactivex.annotations.NonNull;
import io.reactivex.annotations.Nullable;
import io.reactivex.disposables.Disposable;
import io.reactivex.subjects.BehaviorSubject;
import io.reactivex.subjects.PublishSubject;
import ua.naiksoftware.stomp.dto.LifecycleEvent;
import ua.naiksoftware.stomp.dto.StompCommand;
import ua.naiksoftware.stomp.dto.StompHeader;
import ua.naiksoftware.stomp.dto.StompMessage;
import ua.naiksoftware.stomp.pathmatcher.PathMatcher;
import ua.naiksoftware.stomp.pathmatcher.SimplePathMatcher;
import ua.naiksoftware.stomp.provider.ConnectionProvider;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;


/**
 * Created by naik on 05.05.16.
 *
 * @since 2021-04-27
 */
public class StompClient {
    /**
     * SUPPORTED_VERSIONS  usr in StompHeader
     */
    public static final String SUPPORTED_VERSIONS = "1.1,1.2";
    /**
     * DEFAULT_ACK use in StompHeader
     */
    public static final String DEFAULT_ACK = "auto";


    private final ConnectionProvider connectionProvider;
    private ConcurrentHashMap<String, String> topics;
    private boolean legacyWhitespace;

    private PublishSubject<StompMessage> messageStream;
    private BehaviorSubject<Boolean> connectionStream;
    private ConcurrentHashMap<String, Flowable<StompMessage>> streamMap;
    private PathMatcher pathMatcher;
    private Disposable lifecycleDisposable;
    private Disposable messagesDisposable;
    private PublishSubject<LifecycleEvent> lifecyclePublishSubject;
    private List<StompHeader> headers;
    private HeartBeatTask heartBeatTask;


    /**
     * 构造函数
     * *
     *
     * @param provider
     */
    public StompClient(ConnectionProvider provider) {
        this.connectionProvider = provider;
        streamMap = new ConcurrentHashMap<>();
        lifecyclePublishSubject = PublishSubject.create();
        pathMatcher = new SimplePathMatcher();
        heartBeatTask = new HeartBeatTask(this::sendHeartBeat, () -> {
            lifecyclePublishSubject.onNext(new LifecycleEvent(LifecycleEvent.Type.FAILED_SERVER_HEARTBEAT));
        });
    }

    private synchronized PublishSubject<StompMessage> getMessageStream() {
        if (messageStream == null || messageStream.hasComplete()) {
            messageStream = PublishSubject.create();
        }
        return messageStream;
    }

    /**
     * Sets the heartbeat interval to request from the server.
     * <p>
     * Not very useful yet, because we don't have any heartbeat logic on our side.
     *
     * @param ms heartbeat time in milliseconds
     * @return StompClient
     */
    public StompClient withServerHeartbeat(int ms) {
        heartBeatTask.setServerHeartbeat(ms);
        return this;
    }

    /**
     * Sets the heartbeat interval that client propose to send.
     * <p>
     * Not very useful yet, because we don't have any heartbeat logic on our side.
     *
     * @param ms heartbeat time in milliseconds
     * @return StompClient
     */
    public StompClient withClientHeartbeat(int ms) {
        heartBeatTask.setClientHeartbeat(ms);
        return this;
    }

    /**
     * Connect without reconnect if connected
     */
    public void connect() {
        connect(null);
    }

    /**
     * Connect to websocket. If already connected, this will silently fail.
     *
     * @param headerList HTTP headers to send in the INITIAL REQUEST, i.e. during the protocol upgrade
     * @noinspection checkstyle:HiddenField
     */
    public void connect(@Nullable List<StompHeader> headerList) {


        this.headers = headerList;

        if (isConnected()) {
            return;
        }
        lifecycleDisposable = connectionProvider.lifecycle()
                .subscribe(lifecycleEvent -> {
                    switch (lifecycleEvent.getType()) {
                        case OPENED:
                            List<StompHeader> head = new ArrayList<>();
                            head.add(new StompHeader(StompHeader.VERSION, SUPPORTED_VERSIONS));
                            head.add(new StompHeader(StompHeader.HEART_BEAT,
                                    heartBeatTask.getClientHeartbeat() + "," + heartBeatTask.getServerHeartbeat()));
                            if (headerList != null) {
                                head.addAll(headerList);
                            }
                            connectionProvider.send(new StompMessage(StompCommand.CONNECT, head, null)
                                    .compile(legacyWhitespace))
                                    .subscribe(() -> {
                                        lifecyclePublishSubject.onNext(lifecycleEvent);
                                    });
                            break;

                        case CLOSED:
                            disconnect();
                            break;

                        case ERROR:
                            lifecyclePublishSubject.onNext(lifecycleEvent);
                            break;
                        default:
                            break;
                    }
                });

        messagesDisposable = connectionProvider.messages()
                .map(StompMessage::from)
                .filter(heartBeatTask::consumeHeartBeat)
                .doOnNext(getMessageStream()::onNext)
                .filter(msg -> msg.getStompCommand().equals(StompCommand.CONNECTED))
                .subscribe(stompMessage -> {
                    getConnectionStream().onNext(true);
                }, onError -> {
                });
    }

    private synchronized BehaviorSubject<Boolean> getConnectionStream() {
        if (connectionStream == null || connectionStream.hasComplete()) {
            connectionStream = BehaviorSubject.createDefault(false);
        }
        return connectionStream;
    }

    /**
     * send send some
     *
     * @param destination
     * @return Completable
     */
    public Completable send(String destination) {
        return send(destination, null);
    }

    /**
     * send send StompMessage
     *
     * @param destination
     * @param data
     * @return Completable
     */
    public Completable send(String destination, String data) {
        return send(new StompMessage(
                StompCommand.SEND,
                Collections.singletonList(new StompHeader(StompHeader.DESTINATION, destination)),
                data));
    }

    /**
     * send send Message
     *
     * @param stompMessage
     * @return Completable
     */
    public Completable send(@NonNull StompMessage stompMessage) {
        Completable completable = connectionProvider.send(stompMessage.compile(legacyWhitespace));
        CompletableSource connectionComplete = getConnectionStream()
                .filter(isConnected -> isConnected)
                .firstElement().ignoreElement();
        return completable
                .startWith(connectionComplete);
    }

    /**
     * sendHeartBeat
     *
     * @param pingMessage
     */
    private void sendHeartBeat(@NonNull String pingMessage) {
        Completable completable = connectionProvider.send(pingMessage);
        CompletableSource connectionComplete = getConnectionStream()
                .filter(isConnected -> isConnected)
                .firstElement().ignoreElement();
        completable.startWith(connectionComplete)
                .onErrorComplete()
                .subscribe();
    }

    /**
     * lifecycle
     *
     * @return Flowable
     */
    public Flowable<LifecycleEvent> lifecycle() {
        return lifecyclePublishSubject.toFlowable(BackpressureStrategy.BUFFER);
    }

    /**
     * Disconnect from server, and then reconnect with the last-used headers
     */
    public void reconnect() {
        disconnectCompletable()
                .subscribe(() -> connect(headers),
                        error -> {
                        });
    }

    /**
     * disconnect
     *
     * @return boolean
     */
    public boolean disconnect() {
        AtomicBoolean flag = new AtomicBoolean(false);
        disconnectCompletable().subscribe(() -> {
            flag.set(true);
        }, error -> {
        });
        return flag.get();
    }

    /**
     * disconnectCompletable 断开
     *
     * @return Completable
     */
    public Completable disconnectCompletable() {

        heartBeatTask.shutdown();

        if (lifecycleDisposable != null) {
            lifecycleDisposable.dispose();
        }
        if (messagesDisposable != null) {
            messagesDisposable.dispose();
        }

        return connectionProvider.disconnect()
                .doFinally(() -> {

                    getConnectionStream().onComplete();
                    getMessageStream().onComplete();
                    lifecyclePublishSubject.onNext(new LifecycleEvent(LifecycleEvent.Type.CLOSED));
                });
    }

    /**
     * tpic(String,List)
     * *
     *
     * @param destinationPath
     * @return Flowable
     */
    public Flowable<StompMessage> topic(String destinationPath) {
        return topic(destinationPath, null);
    }

    /**
     * streamMap put
     *
     * @param destPath
     * @param headerList
     * @return Flowable
     */
    public Flowable<StompMessage> topic(@NonNull String destPath, List<StompHeader> headerList) {
        if (destPath == null) {
            return Flowable.error(new IllegalArgumentException("Topic path cannot be null"));
        } else if (!streamMap.containsKey(destPath)) {
            streamMap.put(destPath,
                    Completable.defer(() -> subscribePath(destPath, headerList)).andThen(
                            getMessageStream()
                                    .filter(msg -> pathMatcher.matches(destPath, msg))
                                    .toFlowable(BackpressureStrategy.BUFFER)
                                    .doFinally(() -> unsubscribePath(destPath).subscribe())
                                    .share())
            );
        }
        return streamMap.get(destPath);
    }

    private Completable subscribePath(String destinationPath, @Nullable List<StompHeader> headerList) {
        String topicId = UUID.randomUUID().toString();

        if (topics == null) {
            topics = new ConcurrentHashMap<>();
        }

        /**
         * Only continue if we don't already have a subscription to the topic
         */
        if (topics.containsKey(destinationPath)) {
            return Completable.complete();
        }

        topics.put(destinationPath, topicId);
        List<StompHeader> heads = new ArrayList<>();
        heads.add(new StompHeader(StompHeader.ID, topicId));
        heads.add(new StompHeader(StompHeader.DESTINATION, destinationPath));
        heads.add(new StompHeader(StompHeader.ACK, DEFAULT_ACK));
        if (headerList != null) {
            heads.addAll(headerList);
        }
        return send(new StompMessage(StompCommand.SUBSCRIBE,
                heads, null))
                .doOnError(throwable -> unsubscribePath(destinationPath).subscribe());
    }


    private Completable unsubscribePath(String dest) {
        streamMap.remove(dest);

        String topicId = topics.get(dest);

        if (topicId == null) {
            return Completable.complete();
        }

        topics.remove(dest);


        return send(new StompMessage(StompCommand.UNSUBSCRIBE,
                Collections.singletonList(new StompHeader(StompHeader.ID, topicId)), null)).onErrorComplete();
    }

    /**
     * Set the wildcard or other matcher for Topic subscription.
     * <p>
     * Right now, the only options are simple, rmq supported.
     * But you can write you own matcher by implementing {@link PathMatcher}
     * <p>
     * When set to {@link ua.naiksoftware.stomp.pathmatcher.RabbitPathMatcher},
     * topic subscription allows for RMQ-style wildcards.
     * <p>
     *
     * @param matcher Set to {@link SimplePathMatcher} by default
     */
    public void setPathMatcher(PathMatcher matcher) {
        this.pathMatcher = matcher;
    }

    /**
     * isConnected
     *
     * @return boolean
     */
    public boolean isConnected() {
        return getConnectionStream().getValue();
    }

    /**
     * Reverts to the old frame formatting, which included two newlines between the message body
     * and the end-of-frame marker.
     * <p>
     * Legacy: Body
     * <p>
     * Default: Body^@
     *
     * @param ble whether to append an extra two newlines
     * @see <a href="http://stomp.github.io/stomp-specification-1.2.html#STOMP_Frames">The STOMP spec</a>
     */
    public void setLegacyWhitespace(boolean ble) {
        this.legacyWhitespace = ble;
    }

    /**
     * returns the to topic (subscription id) corresponding to a given destination
     *
     * @param dest the destination
     * @return the topic (subscription id) or null if no topic correspon   ds to the destination
     */
    public String getTopicId(String dest) {
        return topics.get(dest);
    }
}
