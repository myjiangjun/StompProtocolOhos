package ua.naiksoftware.stomp.dto;


import java.util.TreeMap;

/**
 * Created by naik on 05.05.16.
 *
 * @since 2021-04-27
 */
public class LifecycleEvent {
    /**
     * Type
     *
     * @since 2021-04-27
     */
    public enum Type {
        /**
         * OPENED, CLOSED, ERROR, FAILED_SERVER_HEARTBEAT
         */
        OPENED, CLOSED, ERROR, FAILED_SERVER_HEARTBEAT
    }

    private final Type mType;

    /**
     * Nullable
     */
    private Exception mException;

    /**
     * Nullable
     */
    private String mMessage;

    private TreeMap<String, String> handshakeResponseHeaders = new TreeMap<>();

    /**
     * LifecycleEvent
     *
     * @param type
     */
    public LifecycleEvent(Type type) {
        mType = type;
    }

    /**
     * LifecycleEvent
     *
     * @param type
     * @param exception
     */
    public LifecycleEvent(Type type, Exception exception) {
        mType = type;
        mException = exception;
    }

    /**
     * LifecycleEvent
     *
     * @param type
     * @param message
     */
    public LifecycleEvent(Type type, String message) {
        mType = type;
        mMessage = message;
    }

    /**
     * getType
     *
     * @return Type
     */
    public Type getType() {
        return mType;
    }

    /**
     * getException
     *
     * @return Exception
     */
    public Exception getException() {
        return mException;
    }

    /**
     * getMessage
     *
     * @return String
     */
    public String getMessage() {
        return mMessage;
    }

    /**
     * setHandshakeResponseHeaders
     *
     * @param map setHandshakeResponseHeaders
     */
    public void setHandshakeResponseHeaders(TreeMap<String, String> map) {
        this.handshakeResponseHeaders = map;
    }

    /**
     * getHandshakeResponseHeaders
     *
     * @return TreeMap
     */
    public TreeMap<String, String> getHandshakeResponseHeaders() {
        return handshakeResponseHeaders;
    }
}
